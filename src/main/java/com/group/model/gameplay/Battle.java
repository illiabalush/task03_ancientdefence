package com.group.model.gameplay;

import com.group.controller.Controller;
import com.group.model.object.Player;
import com.group.model.unit.Unit;

import java.util.Iterator;
import java.util.Random;

public class Battle {
    private Controller controller;
    private boolean isFirstPlayerTurn;

    public Battle() {
        controller = new Controller();
    }

    public void startBattle(Player player1, Player player2) {
        if (player1.getGeneralAgility() > player2.getGeneralAgility()) {
            isFirstPlayerTurn = true;
            attack(player1, player2);
        } else if (player1.getGeneralAgility() < player2.getGeneralAgility()) {
            isFirstPlayerTurn = false;
            attack(player2, player1);
        } else if (player1.getGeneralAgility() == player2.getGeneralAgility()) {
            if (new Random().nextInt(10) >= 5) {
                isFirstPlayerTurn = true;
                attack(player1, player2);
            } else {
                isFirstPlayerTurn = false;
                attack(player2, player1);
            }
        }
    }

    private Player attack(Player attacker, Player defender) {
        if (isWinner(attacker, defender)) {
            controller.getView().showEndBattle(getWinner(attacker, defender));
            return getWinner(attacker, defender);
        }
        controller.getView().showBattle(attacker, defender);
        controller.selectBonus(attacker);
        controller.selectAttackTarget(attacker, defender);
        try {
            Thread.sleep(1500);
        } catch (InterruptedException ignored) {
        }
        reduceHealth(attacker, defender);
        isFirstPlayerTurn = !isFirstPlayerTurn;
        return isFirstPlayerTurn ? attack(defender, attacker) : attack(defender, attacker);
    }

    private Player getWinner(Player player1, Player player2) {
        return player1.getGeneralHealth() <= 0 ? player2 : player1;
    }

    private void reduceHealth(Player attacker, Player defender) {
        if (!isMiss(defender)) {
            double attackerDamage = attacker.getGeneralDamage() -
                    attacker.getGeneralDamage() * defender.getGeneralArmor();
            Iterator<Unit> iterator = defender.getUnitsList().iterator();
            while (iterator.hasNext()) {
                Unit nextUnit = iterator.next();
                if (attackerDamage >= nextUnit.getHealth()) {
                    attackerDamage -= nextUnit.getHealth();
                    attacker.setMoney(attacker.getMoney() + nextUnit.getMoneyAfterDeath());
                    iterator.remove();
                } else {
                    nextUnit.setHealth(nextUnit.getHealth() - attackerDamage);
                    return;
                }
            }
        } else {
            System.out.println(attacker.getName().toUpperCase() + " IS MISSING");
        }
    }

    private boolean isMiss(Player defender) {
        return new Random().nextInt(100) <= defender.getGeneralIntelligence();
    }

    private boolean isWinner(Player player1, Player player2) {
        return player1.getGeneralHealth() <= 0 || player2.getGeneralHealth() <= 0;
    }
}
