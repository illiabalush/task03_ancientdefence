package com.group.model.gameplay;

import com.group.model.object.Player;
import com.group.model.unit.Unit;

public class Bonus {

    public boolean addHealth(Player player) {
        if (player.getMoney() >= 150) {
            player.setMoney(player.getMoney() - 150);
            for (Unit i : player.getUnitsList()) {
                i.setHealth(i.getHealth() + 300 / player.getUnitsList().size());
            }
            return true;
        } else {
            return false;
        }
    }

    public boolean setDoubleDamage(Player player) {
        if (player.getMoney() >= 200) {
            player.setMoney(player.getMoney() - 200);
            for (Unit i : player.getUnitsList()) {
                i.setDamage(i.getDamage() * 2);
            }
            return true;
        } else {
            return false;
        }
    }
}
