package com.group.model.gameplay;

import com.group.model.object.Player;

public class Sorter {
    private Player player;

    public Sorter(Player player) {
        this.player = player;
    }

    public void sortByHealth() {
        player.getUnitsList().sort((o1, o2) -> (o1.getHealth() > o2.getHealth()) ? -1 : 0);
    }

    public void sortByDamage() {
        player.getUnitsList().sort((o1, o2) -> (o1.getDamage() > o2.getDamage()) ? -1 : 0);
    }

    public void sortByIntelligence() {
        player.getUnitsList().sort((o1, o2) -> (o1.getIntelligence() > o2.getIntelligence()) ? -1 : 0);
    }

    public void sortByMoney() {
        player.getUnitsList().sort((o1, o2) -> (o1.getMoneyAfterDeath() > o2.getMoneyAfterDeath()) ? -1 : 0);
    }
}
