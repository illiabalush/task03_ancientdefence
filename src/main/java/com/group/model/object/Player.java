package com.group.model.object;

import com.group.model.unit.Unit;

import java.util.ArrayList;

public class Player {
    private String name;
    private int money;
    private ArrayList<Unit> unitsList;
    private Unit race;

    public Player() {
        unitsList = new ArrayList<>();
        money = 500;
    }

    public void setRace(Unit race) {
        this.race = race;
    }

    public Unit getRace() {
        return race;
    }

    ;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public ArrayList<Unit> getUnitsList() {
        return unitsList;
    }

    public double getGeneralHealth() {
        double generalHealth = 0;
        for (Unit i : unitsList) {
            generalHealth += i.getHealth();
        }
        return generalHealth;
    }

    public double getGeneralArmor() {
        double generalArmor = 0;
        for (Unit i : unitsList) {
            generalArmor += i.getArmor();
        }
        return generalArmor;
    }

    public double getGeneralAgility() {
        double generalAgility = 0;
        for (Unit i : unitsList) {
            generalAgility += i.getAgility();
        }
        return generalAgility;
    }

    public double getGeneralDamage() {
        double generalDamage = 0;
        for (Unit i : unitsList) {
            generalDamage += i.getDamage();
        }
        return generalDamage;
    }

    public double getGeneralIntelligence() {
        double generalIntelligence = 0;
        for (Unit i : unitsList) {
            generalIntelligence += i.getIntelligence();
        }
        return generalIntelligence;
    }

    public boolean addUnit(Unit unit) {
        if (money < unit.getCost()) {
            return false;
        } else {
            money -= unit.getCost();
            unitsList.add(unit);
            return true;
        }
    }
}