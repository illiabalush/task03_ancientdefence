package com.group.model.unit.gnome;

import com.group.model.unit.Image;

public class Gungnome extends Gnome {
    public Gungnome() {
        health = 200 + bonusHealth;
        armor = 0.01 + bonusArmor;
        damage = 110;
        intelligence = 1;
        agility = 30;
        cost = 70;
        moneyAfterDeath = cost / 5;
        image = Image.gunGnome;
    }
}
