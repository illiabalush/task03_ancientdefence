package com.group.model.unit.gnome;

import com.group.model.unit.Image;

public class Miner extends Gnome {
    public Miner() {
        health = 100 + bonusHealth;
        armor = 0.008 + bonusArmor;
        damage = 50;
        intelligence = 2;
        agility = 50;
        cost = 40;
        moneyAfterDeath = cost / 5;
        image = Image.gnomeMiner;
    }
}
