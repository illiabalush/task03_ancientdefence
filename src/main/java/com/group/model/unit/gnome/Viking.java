package com.group.model.unit.gnome;

import com.group.model.unit.Image;

public class Viking extends Gnome {
    public Viking() {
        health = 300 + bonusHealth;
        armor = 0.04 + bonusArmor;
        damage = 95;
        intelligence = 2;
        agility = 55;
        cost = 110;
        moneyAfterDeath = cost / 5;
        image = Image.gnomeViking;
    }
}
