package com.group.model.unit.gnome;

import com.group.model.unit.Unit;

public class Gnome extends Unit {
    double bonusHealth = 70;
    double bonusArmor = 0.1;

    public static void showAllGnomeUnits() {
        new Gungnome().showUnit("GunGnome");
        new Miner().showUnit("Miner");
        new Tank().showUnit("Tank");
        new Viking().showUnit("Viking");
    }
}
