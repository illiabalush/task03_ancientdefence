package com.group.model.unit.gnome;

import com.group.model.unit.Image;

public class Tank extends Gnome {
    public Tank() {
        health = 300 + bonusHealth;
        armor = 0.025 + bonusArmor;
        damage = 50;
        intelligence = 0.5;
        agility = 20;
        cost = 70;
        moneyAfterDeath = cost / 5;
        image = Image.gnomeTank;
    }
}
