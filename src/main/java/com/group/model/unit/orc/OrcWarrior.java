package com.group.model.unit.orc;

import com.group.model.unit.Image;

public class OrcWarrior extends Orc {
    public OrcWarrior() {
        health = 60;
        armor = 0.006 + bonusArmor;
        damage = 30 + bonusDamage;
        intelligence = 0.5;
        agility = 50;
        cost = 30;
        moneyAfterDeath = cost / 5;
        image = Image.orcWarrior;
    }
}
