package com.group.model.unit.orc;

import com.group.model.unit.Unit;

public class Orc extends Unit {
    double bonusArmor = 0.1;
    double bonusDamage = 30;

    public static void showAllOrcUnits() {
        new Berserker().showUnit("Berserker");
        new Hunter().showUnit("Hunter");
        new OrcWarrior().showUnit("Orc Warrior");
        new Shaman().showUnit("Shaman");
    }
}
