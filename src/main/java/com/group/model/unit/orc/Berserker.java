package com.group.model.unit.orc;

import com.group.model.unit.Image;

public class Berserker extends Orc {
    public Berserker() {
        health = 120;
        armor = 0.02 + bonusArmor;
        damage = 60 + bonusDamage;
        intelligence = 1;
        agility = 40;
        cost = 60;
        moneyAfterDeath = cost / 5;
        image = Image.berserk;
    }
}
