package com.group.model.unit.orc;

import com.group.model.unit.Image;

public class Shaman extends Orc {
    public Shaman() {
        health = 300;
        armor = 0 + bonusArmor;
        damage = 25 + bonusDamage;
        intelligence = 4;
        agility = 70;
        cost = 50;
        moneyAfterDeath = cost / 5;
        image = Image.shaman;
    }
}
