package com.group.model.unit.orc;

import com.group.model.unit.Image;

public class Hunter extends Orc {
    public Hunter() {
        health = 200;
        armor = 0.025 + bonusArmor;
        damage = 90 + bonusDamage;
        intelligence = 2;
        agility = 60;
        cost = 100;
        moneyAfterDeath = cost / 5;
        image = Image.hunter;
    }
}
