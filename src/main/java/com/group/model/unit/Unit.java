package com.group.model.unit;

abstract public class Unit {
    protected double health;
    protected double armor;
    protected double agility;
    protected double damage;
    protected double intelligence;
    protected int cost;
    protected String[] image;
    protected int moneyAfterDeath = cost / 5;

    public double getHealth() {
        return health;
    }

    public void setHealth(double health) {
        this.health = health;
    }

    public double getArmor() {
        return armor;
    }

    public void setArmor(double armor) {
        this.armor = armor;
    }

    public double getAgility() {
        return agility;
    }

    public void setAgility(double agility) {
        this.agility = agility;
    }

    public double getDamage() {
        return damage;
    }

    public void setDamage(double damage) {
        this.damage = damage;
    }

    public double getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(double intelligence) {
        this.intelligence = intelligence;
    }

    public int getCost() {
        return cost;
    }

    public String getImage(int number) {
        return image[number];
    }

    public int getMoneyAfterDeath() {
        return moneyAfterDeath;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public void setMoneyAfterDeath(int moneyAfterDeath) {
        this.moneyAfterDeath = moneyAfterDeath;
    }

    public void showUnit(String name) {
        String tab = "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
        System.out.println(image[0] + tab + "\tName: " + name);
        System.out.println(image[1] + tab + "Health points: " + damage);
        System.out.println(image[2] + tab + "Armor : " + (float)armor);
        System.out.println(image[3] + tab + "Damage : " + damage);
        System.out.println(image[4] + tab + "Intelligence : " + intelligence);
        System.out.println(image[5] + tab + "Agility : " + agility);
        System.out.println(image[6] + tab + "\tCost : " + cost);
        System.out.println();
    }
}

