package com.group.model.unit.human;

import com.group.model.unit.Image;

public class Wizard extends Human {
    public Wizard() {
        health = 250;
        armor = 0;
        damage = 20;
        intelligence = 4 + bonusIntelligence;
        agility = 40 + bonusAgility;
        cost = 60;
        moneyAfterDeath = cost / 5;
        image = Image.wizard;
    }
}
