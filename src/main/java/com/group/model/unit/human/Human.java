package com.group.model.unit.human;

import com.group.model.unit.Unit;

public class Human extends Unit {
    double bonusIntelligence = 2.5;
    double bonusAgility = 20;

    public static void showAllHumanUnits() {
        new Bowman().showUnit("Bowman");
        new Cavalry().showUnit("Cavalry");
        new Swordsman().showUnit("Swordsman");
        new Wizard().showUnit("Wizard");
    }
}
