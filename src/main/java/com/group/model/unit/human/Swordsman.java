package com.group.model.unit.human;

import com.group.model.unit.Image;

public class Swordsman extends Human {
    public Swordsman() {
        health = 70;
        armor = 0.02;
        damage = 45;
        intelligence = 1 + bonusIntelligence;
        agility = 30 + bonusAgility;
        cost = 30;
        moneyAfterDeath = cost / 5;
        image = Image.swordsman;
    }
}
