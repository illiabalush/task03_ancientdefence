package com.group.model.unit.human;

import com.group.model.unit.Image;

public class Bowman extends Human {
    public Bowman() {
        health = 100;
        armor = 0.015;
        damage = 145;
        intelligence = 2 + bonusIntelligence;
        agility = 60 + bonusAgility;
        cost = 80;
        moneyAfterDeath = cost / 5;
        image = Image.bowman;
    }
}
