package com.group.model.unit.human;

import com.group.model.unit.Image;

public class Cavalry extends Human {
    public Cavalry() {
        health = 290;
        armor = 0.055;
        damage = 135;
        intelligence = 3 + bonusIntelligence;
        agility = 60 + bonusAgility;
        cost = 150;
        moneyAfterDeath = cost / 5;
        image = Image.cavalry;
    }
}
