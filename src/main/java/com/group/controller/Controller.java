package com.group.controller;

import com.group.model.gameplay.Battle;
import com.group.model.gameplay.Sorter;
import com.group.model.gameplay.Bonus;
import com.group.model.object.Player;
import com.group.model.unit.gnome.*;
import com.group.model.unit.human.*;
import com.group.model.unit.orc.*;
import com.group.view.View;

import java.util.Scanner;

public class Controller {
    private View view;
    private Scanner scanner;

    public Controller() {
        view = new View();
        scanner = new Scanner(System.in);
    }

    public void start() {
        Player player1 = new Player();
        createPlayer(player1);
        Player player2 = new Player();
        createPlayer(player2);
        new Battle().startBattle(player1, player2);
    }

    private void createPlayer(Player player) {
        view.showStartMenu();
        selectName(player);
        selectRace(player);
    }

    private void selectName(Player player) {
        scanner = new Scanner(System.in);
        System.out.println("\nSelect name: ");
        player.setName(scanner.nextLine());
    }

    private void selectRace(Player player) {
        view.showRace();
        System.out.println("\nSelect race: ");
        while (true) {
            String race = scanner.nextLine();
            switch (race) {
                case "1":
                    player.setRace(new Orc());
                    selectOrcs(player);
                    return;
                case "2":
                    player.setRace(new Human());
                    selectHumans(player);
                    return;
                case "3":
                    player.setRace(new Gnome());
                    selectGnomes(player);
                    return;
                default:
                    break;
            }
        }
    }

    private void selectOrcs(Player player) {
        view.showUnits(player);
        System.out.println("\nSelect units: ");
        while (true) {
            System.out.println("You have " + player.getMoney() + " $");
            String unit = scanner.nextLine();
            switch (unit) {
                case "1":
                    if (!player.addUnit(new Berserker())) {
                        System.out.println("Not enough money");
                    }
                    break;
                case "2":
                    if (!player.addUnit(new Hunter())) {
                        System.out.println("Not enough money");
                    }
                    break;
                case "3":
                    if (!player.addUnit(new OrcWarrior())) {
                        System.out.println("Not enough money");
                    }
                    break;
                case "4":
                    if (!player.addUnit(new Shaman())) {
                        System.out.println("Not enough money");
                    }
                    break;
                case "0":
                    return;
                default:
                    break;
            }
        }
    }

    private void selectHumans(Player player) {
        view.showUnits(player);
        System.out.println("\nSelect units: ");
        while (true) {
            System.out.println("You have " + player.getMoney() + " $");
            String unit = scanner.nextLine();
            switch (unit) {
                case "1":
                    if (!player.addUnit(new Bowman())) {
                        System.out.println("Not enough money");
                    }
                    break;
                case "2":
                    if (!player.addUnit(new Cavalry())) {
                        System.out.println("Not enough money");
                    }
                    break;
                case "3":
                    if (!player.addUnit(new Swordsman())) {
                        System.out.println("Not enough money");
                    }
                    break;
                case "4":
                    if (!player.addUnit(new Wizard())) {
                        System.out.println("Not enough money");
                    }
                    break;
                case "0":
                    return;
                default:
                    break;
            }
        }
    }

    private void selectGnomes(Player player) {
        view.showUnits(player);
        System.out.println("\nSelect units: ");
        while (true) {
            System.out.println("You have " + player.getMoney() + " $");
            String unit = scanner.nextLine();
            switch (unit) {
                case "1":
                    if (!player.addUnit(new Gungnome())) {
                        System.out.println("Not enough money");
                    }
                    break;
                case "2":
                    if (!player.addUnit(new Miner())) {
                        System.out.println("Not enough money");
                    }
                    break;
                case "3":
                    if (!player.addUnit(new Tank())) {
                        System.out.println("Not enough money");
                    }
                    break;
                case "4":
                    if (!player.addUnit(new Viking())) {
                        System.out.println("Not enough money");
                    }
                    break;
                case "0":
                    return;
                default:
                    break;
            }
        }
    }

    public void selectBonus(Player player) {
        view.showBonus();
        System.out.println("\t\t\t\t" + player.getName().toUpperCase() + " TURN");
        System.out.println("\nSelect bonus: ");
        while (true) {
            System.out.println("You have " + player.getMoney() + " $");
            String bonus = scanner.nextLine();
            switch (bonus) {
                case "1":
                    if (!new Bonus().setDoubleDamage(player)) {
                        System.out.println("Not enough money");
                    }
                    break;
                case "2":
                    if (!new Bonus().addHealth(player)) {
                        System.out.println("Not enough money");
                    }
                    break;
                case "0":
                    return;
                default:
                    break;
            }
        }
    }

    public void selectAttackTarget(Player attacker, Player defender) {
        view.showAttackTarget();
        System.out.println("\t\t\t\t" + attacker.getName().toUpperCase() + " TURN");
        System.out.println("\nSelect attack target: ");
        while (true) {
            String parameter = scanner.nextLine();
            switch (parameter) {
                case "1":
                    new Sorter(defender).sortByHealth();
                    return;
                case "2":
                    new Sorter(defender).sortByDamage();
                    return;
                case "3":
                    new Sorter(defender).sortByIntelligence();
                    return;
                case "4":
                    new Sorter(defender).sortByMoney();
                    return;
                case "0":
                    return;
                default:
                    break;
            }
        }
    }

    public View getView() {
        return view;
    }
}
