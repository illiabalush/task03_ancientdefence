package com.group.view;

import com.group.controller.Controller;
import com.group.model.object.Player;
import com.group.model.unit.gnome.Gnome;
import com.group.model.unit.human.Human;
import com.group.model.unit.orc.Orc;

public class View {
    private static String tab = "\t\t\t\t\t\t\t\t\t\t\t\t";

    public void start() {
        new Controller().start();
    }

    public void showStartMenu() {
        System.out.println("                                                  !_\n" +
                "                                                    |*~=-.,\n" +
                "                        AncientDefence              |_,-'`\n" +
                "                                                    |\n" +
                "                                                    |\n" +
                "                                                   /^\\\n" +
                "                     !_                           /   \\\n" +
                "                     |*`~-.,                     /,    \\\n" +
                "                     |.-~^`                     /#\"     \\\n" +
                "                     |                        _/##_   _  \\_\n" +
                "                _   _|  _   _   _            [ ]_[ ]_[ ]_[ ]\n" +
                "               [ ]_[ ]_[ ]_[ ]_[ ]            |_=_-=_ - =_|\n" +
                "             !_ |_=_ =-_-_  = =_|           !_ |=_= -    |\n" +
                "             |*`--,_- _        |            |*`~-.,= []  |\n" +
                "             |.-'|=     []     |   !_       |_.-\"`_-     |\n" +
                "             |   |_=- -        |   |*`~-.,  |  |=_-      |\n" +
                "            /^\\  |=_= -        |   |_,-~`  /^\\ |_ - =[]  |\n" +
                "        _  /   \\_|_=- _   _   _|  _|  _   /   \\|=_-      |\n" +
                "       [ ]/,    \\[ ]_[ ]_[ ]_[ ]_[ ]_[ ]_/,    \\[ ]=-    |" + tab + "HELLO AND WELCOME TO THE ANCIENT DEFENCE GAME  \n" +
                "        |/#\"     \\_=-___=__=__- =-_ -=_ /#\"     \\| _ []  |\\" + tab + "YOU CAN PLAY WITH YOUR FRIEND AND BATTLE TO SEE\n" +
                "       _/##_   _  \\_-_ =  _____       _/##_   _  \\_ -    |\\\\" + tab + "WHO IS BETTER AT STRATEGY\n" +
                "      [ ]_[ ]_[ ]_[ ]=_0~{_ _ _}~0   [ ]_[ ]_[ ]_[ ]=-   | \\\n" +
                "      |_=__-_=-_  =_|-=_ |  ,  |     |_=-___-_ =-__|_    |  \\\\\t" + tab + "PLEASE ENTER YOUR NAME TO START \n" +
                "       | _- =-     |-_   | ((* |      |= _=       | -    |___\\\n" +
                "       |= -_=      |=  _ |  `  |      |_-=_       |=_    |/+\\|\n" +
                "       | =_  -     |_ = _ `-.-`       | =_ = =    |=_-   ||+||\n" +
                "       |-_=- _     |=_   =            |=_= -_     |  =   ||+||\n" +
                "       |=_- /+\\    | -=               |_=- /+\\    |=_    |^^^|\n" +
                "       |=_ |+|+|   |= -  -_,--,_      |_= |+|+|   |  -_  |=  |\n" +
                "       |  -|+|+|   |-_=  / |  | \\     |=_ |+|+|   |-=_   |_-/\n" +
                "       |=_=|+|+|   | =_= | |  | |     |_- |+|+|   |_ =   |=/\n" +
                "       | _ ^^^^^   |= -  | |  <&>     |=_=^^^^^   |_=-   |/\n" +
                "       |=_ =       | =_-_| |  | |     |   =_      | -_   |\n" +
                "       |_=-_       |=_=  | |  | |     |=_=        |=-    |");
    }

    public void showRace() {
        System.out.println(
                        "      _..;|;__;|;" + tab + "\t\t\t INFO : \n" +
                        "    7;-..     :  )" + tab + "Orcs are dark-skinned creatures with the growth of the average \n" +
                        "  ._)|   `;==,|,=='" + tab + "man, created from the most bloodthirsty animals and tortured by torture.\n" +
                        "  \\`@; \\_ `<`G,\" G)" + tab + "They have bigger DAMAGE and more ARMORE \n" +
                        "  `\\/-;,(  )  .>. )" + tab + "\t\t Press 1 to choose orcs. \n" +
                        "      < ,-;'-.__.;'\n" +
                        "       `\\_ `-,__,'\n" +
                        "        `-..,;,>\n");
        System.out.println(
                        "        __" + tab + "\t\t\t\tINFO : \n" +
                        "     .-'  '-." + tab + "Humans are average peaceful beings who wield not \n" +
                        "    /        )" + tab + "only the sword but also the intelligence. \n" +
                        "    |  C   o( " + tab + "Humans have more intelligence and dexterity.\n" +
                        "     \\       >" + tab + "\t\t Press 2 to choose humans. \n" +
                        "      )  \\  / \n" +
                        "   .-._ / `'\n" +
                        "  / _    \\\n\n");
        System.out.println(
                        "         __ " + tab + "\t\t\t INFO : \n" +
                        "      .-'  |" + tab + "Gnomes are an underground people. They are short and famous for \n" +
                        "     /   <\\|" + tab + "their health and deceit.\n" +
                        "    /     \\'" + tab + "Gnomes have the biggest armor and health point from all races.\n" +
                        "    |_.- o-o " + tab + "\t\t Press 3 to choose gnomes.\n" +
                        "    / C  -._)\\\n" +
                        "   /',        |\n" +
                        "  |   `-,_,__,'");
    }

    public void showUnits(Player player) {
        String playerRace = player.getRace().getClass().getName().
                substring(player.getRace().getClass().getName().lastIndexOf('.') + 1);
        switch (playerRace) {
            case "Human":
                Human.showAllHumanUnits();
                break;
            case "Gnome":
                Gnome.showAllGnomeUnits();
                break;
            case "Orc":
                Orc.showAllOrcUnits();
                break;
        }
    }

    public void showBonus() {
        System.out.println("\n\n" +
                "                                           _.gd8888888bp._\n" +
                "                                        .g88888888888888888p.\n" +
                "                                      .d8888P\"\"       \"\"Y8888b.\n" +
                "                                      \"Y8P\"               \"Y8P'\n" +
                "                                         `.               ,'\n" +
                "                                           \\     .-.     /\n" +
                "                                            \\   (___)   /\n" +
                " .------------------._______________________:__________j\t" + tab + "\t\t\t\t\t\t\tMULTIPLY DAMAGE X 2 \n" +
                "/                   |                      |           |`-.,_\n" +
                "\\###################|######################|###########|,-'`\n" +
                " `------------------'                       :    ___   l\t" + tab + "\t\t\t\t\t\tCost : 200\n" +
                "                                            /   (   )   \\\n" +
                "                                           /     `-'     \\\n" +
                "                                         ,'               `.\n" +
                "                                      .d8b.               .d8b.\n" +
                "                                      \"Y8888p..       ,.d8888P\"\t" + tab + "\t\t\t\t\t\tPress 1 to chose\n" +
                "                                        \"Y88888888888888888P\"\n" +
                "                                           \"\"YY8888888PP\"\"\n\n");
        System.out.println(
                "                              __\n" +
                        "                             /  \\\n" +
                        "                            |    |\n" +
                        "        (`----._____.-'\"\"\"`. \\__/ .'\"\"\"`-._____.----')\n" +
                        "         (____       .      `|  |'      .       ____)\n" +
                        "           (___`----' .     _|  |_     . `----'___)\n" +
                        "             (__`----'  _.-' |  | `-._  `----'__)\n" + tab + tab + "\t\t\t\t\t\tADD 300HP TO YOUR GLOBAL POINTS\n" +
                        "               `._____.'_    |  |    _`._____.'\n" +
                        "                      /o )-< |  | >-( o\\\n" +
                        "                     / .'    |  |    `. \\\t" + tab + "\t\t\t\t\t\tCost : 150\n" +
                        "                    J J      |  |      L L\n" +
                        "                    | |      |  |      | |\n" +
                        "                    J J      |  |      F F\t" + tab + "\t\t\t\t\t\t\t\tPress 2 to chose\n" +
                        "                     \\ \\     |  |     / /\n" +
                        "                      \\ `.   |  |   .' /\n\n");
    }

    public void showAttackTarget() {
        System.out.println(
                        "" +
                        "" +
                        tab +"You can chose how do you wanna attack first :\n " +
                        "" +
                        tab + "Press :\n" +
                        tab + "\t 1 to attack unit with more health\n" +
                        tab + "\t 2 to attack unit with more damage\n" +
                        tab + "\t 3 to attack unit with more intelligence\n" +
                        tab + "\t 4 to attack unit with more money\n\n\n\n" +
                        "" +
                        "" +
                        ""
        );
    }

    private String showPlayerStat(Player player) {
        return " HP : " + (int)player.getGeneralHealth() +
        "| Intelligence : " + (int)player.getGeneralIntelligence() +
        "| Agility : " + (int)player.getGeneralAgility() +
        "| Armor : " + (int)player.getGeneralArmor() +
        "| Damage : " + (int)player.getGeneralDamage() +
        "| Total money : " + player.getMoney();
    }

    public void showBattle(Player attacker, Player defender) {
        System.out.println(showPlayerStat(attacker) + "\t\t\t\t" + showPlayerStat(defender) + "\n\n");
        boolean isAttackerBiggerThanDefender = attacker.getUnitsList().size()
                > defender.getUnitsList().size();
        int smallerList = (!isAttackerBiggerThanDefender) ?
                attacker.getUnitsList().size() : defender.getUnitsList().size();
        int biggerList = (isAttackerBiggerThanDefender) ?
                attacker.getUnitsList().size() : defender.getUnitsList().size();
        for (int k = 0; k < smallerList; k++) {
            for (int i = 0; i < 7; i++) {
                System.out.println(
                        attacker.getUnitsList().get(k).getImage(i)
                                + tab + tab + "\t\t"
                                + defender.getUnitsList().get(k).getImage(i));
            }
            System.out.println("\n");
        }
        for (int i = smallerList; i < biggerList; i++) {
            for (int j = 0; j < 7; j++) {
                if (isAttackerBiggerThanDefender) {
                    System.out.println(attacker.getUnitsList().get(i).getImage(j));
                } else {
                    System.out.println(tab + tab + tab + defender.getUnitsList().get(i).getImage(j));
                }
            }
            System.out.println("\n");
        }
    }

    public void showEndBattle(Player player) {
        System.out.println("\n\n" + tab + "CONGRATULATIONS " + player.getName().toUpperCase() + " YOU WIN");
    }
}
