package com.group.application;

import com.group.view.View;

public class Main {
    public static void main(String[] args) {
        new View().start();
    }
}
